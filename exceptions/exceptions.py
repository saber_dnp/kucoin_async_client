# coding=utf-8

import json

# System error codes
# Code	Meaning
# 400001	Any of KC-API-KEY, KC-API-SIGN, KC-API-TIMESTAMP, KC-API-PASSPHRASE is missing in your request header
# 400002	KC-API-TIMESTAMP Invalid -- Time differs from server time by more than 5 seconds
# 400003	KC-API-KEY not exists
# 400004	KC-API-PASSPHRASE error
# 400005	Signature error -- Please check your signature
# 400006	The requested ip address is not in the api whitelist
# 400007	Access Denied -- Your api key does not have sufficient permissions to access the uri
# 404000	Url Not Found -- The request resource could not be found
# 400100	Parameter Error -- You tried to access the resource with invalid parameters
# 411100	User are frozen -- User are frozen, please contact us via support center.
# 500000	Internal Server Error -- We had a problem with our server. Try again later.


class KucoinAPIException(Exception):
    """Exception class to handle general API Exceptions

        `code` values

        `message` format

    """
    def __init__(self, response):
        self.code = ''
        self.message = 'Unknown Error'
        try:
            json_res = response.json()
        except ValueError:
            self.message = response.content
        else:
            if 'error' in json_res:
                self.message = json_res['error']
            if 'msg' in json_res:
                self.message = json_res['msg']
            if 'message' in json_res and json_res['message'] != 'No message available':
                self.message += ' - {}'.format(json_res['message'])
            if 'code' in json_res:
                self.code = json_res['code']
            if 'data' in json_res:
                try:
                    self.message += " " + json.dumps(json_res['data'])
                except ValueError:
                    pass

        self.status_code = response.status_code
        self.response = response
        self.request = getattr(response, 'request', None)

    def __str__(self):  # pragma: no cover
        return 'KucoinAPIException {}: {}'.format(self.code, self.message)


class KucoinRequestException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'KucoinRequestException: {}'.format(self.message)


class KucoinMarketOrderParamsException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'MarketOrderException: {}'.format(self.message)


class KucoinLimitOrderParamsException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return 'LimitOrderException: {}'.format(self.message)


class KucoinGetOrderFailed(Exception):
    pass


class KucoinCancelOrderFailed(Exception):
    pass


class KucoinCreateLimitOrderFailed(Exception):
    pass


class KucoinOrderNotExistOrNotAllowedToCancel(Exception):
    pass


class KucoinCreateMarketOrderFailed(Exception):
    pass


class KucoinGetTimeStampFailed(Exception):
    pass


class KucoinGetStatusFailed(Exception):
    pass


class KucoinGetCurrenciesFailed(Exception):
    pass


class KucoinGetCurrencyFailed(Exception):
    pass


class KucoinGetAccountsFailed(Exception):
    pass


class KucoinGetActualFeeFailed(Exception):
    pass


class KucoinGetAccountFailed(Exception):
    pass


class KucoinCreateAccountFailed(Exception):
    pass


class KucoinGetAccountActivityFailed(Exception):
    pass


class KucoinCreateInnerTransferFailed(Exception):
    pass


class KucoinGetNonZeroTradeAccountsFailed(Exception):
    pass


class KucoinGetSymbolsFailed(Exception):
    pass


class KucoinGetTickerFailed(Exception):
    pass


class KucoinGetFiatPricesFailed(Exception):
    pass


class KucoinGet24HourStatsFailed(Exception):
    pass


class KucoinGetMarketsFailed(Exception):
    pass


class KucoinOrderBookFormatError(Exception):
    pass


class KucoinGetOrderBookFailed(Exception):
    pass


class KucoinGetFullOrderBookFailed(Exception):
    pass


class KucoinGetFullOrderBookLevel3Failed(Exception):
    pass


class KucoinGetTradeHistoriesFailed(Exception):
    pass


class KucoinGetKlineFailed(Exception):
    pass


class KucoinGetWsEndpointFailed(Exception):
    pass


class KucoinCancelOrderByClientOidFailed(Exception):
    pass


class KucoinCancelAllOrdersFailed(Exception):
    pass


class KucoinGetOrdersFailed(Exception):
    pass


class KucoinGetOrderByClientOid(Exception):
    pass


class KucoinGetFillsFailed(Exception):
    pass


class KucoinCreateDepositAddressFailed(Exception):
    pass


class KucoinGetDepositAddressFailed(Exception):
    pass


class KucoinGetDepositsFailed(Exception):
    pass


class KucoinGetWithdrawalsFailed(Exception):
    pass


class KucoinGetWithdrawalQuotasFailed(Exception):
    pass


class KucoinCreateWithdrawFailed(Exception):
    pass


class KucoinCancelWithdrawalFailed(Exception):
    pass


class KucoinGetParamsForSigFailed(Exception):
    pass


class KucoinGenerateSignitureFailed(Exception):
    pass


class KucoinRequestFailed(Exception):
    pass


class KucoinApiResponseNotConvertibleToDict(Exception):
    pass


class KucoinApiResponseCodeNot200000(Exception):
    pass


class KucoinApiResponseSuccessFieldNotTrue(Exception):
    pass


class KucoinCreatePathFailed(Exception):
    pass


class KucoinCreateURIFailed(Exception):
    pass

