This is an unofficial async python client for kucoin api. <br/>
Unofficial Kucoin async client using asyncio library. (use at your own risk.) <br/>
Author: Saber Dinpashoh <br/>
linkedin: https://www.linkedin.com/in/saber-dinpashoh-926b23170/ <br/>
gitlab: https://gitlab.com/saber_dnp <br/>
gitlab_repo: https://gitlab.com/saber_dnp/kucoin_async_client <br/>
