import base64
import calendar
import hashlib
import hmac
import time
from datetime import datetime
import httpx

from .exceptions.exceptions import *
from .utils import compact_json_dict, flat_uuid

"""
Unofficial Kucoin async client using asyncio library. (use at your own risk.)
Author: Saber Dinpashoh
linkedin: https://www.linkedin.com/in/saber-dinpashoh-926b23170/
gitlab: https://gitlab.com/saber_dnp
"""

class KucoinAsyncClient(object):
    REST_API_URL = 'https://openapi-v2.kucoin.com'
    SANDBOX_API_URL = 'https://openapi-sandbox.kucoin.com'
    API_VERSION = 'v1'
    API_VERSION2 = 'v2'
    API_VERSION3 = 'v3'

    SIDE_BUY = 'buy'
    SIDE_SELL = 'sell'

    ACCOUNT_MAIN = 'main'
    ACCOUNT_TRADE = 'trade'

    ORDER_LIMIT = 'limit'
    ORDER_MARKET = 'market'
    ORDER_LIMIT_STOP = 'limit_stop'
    ORDER_MARKET_STOP = 'market_stop'

    STOP_LOSS = 'loss'
    STOP_ENTRY = 'entry'

    STP_CANCEL_NEWEST = 'CN'
    STP_CANCEL_OLDEST = 'CO'
    STP_DECREASE_AND_CANCEL = 'DC'
    STP_CANCEL_BOTH = 'CB'

    TIMEINFORCE_GOOD_TILL_CANCELLED = 'GTC'
    TIMEINFORCE_GOOD_TILL_TIME = 'GTT'
    TIMEINFORCE_IMMEDIATE_OR_CANCEL = 'IOC'
    TIMEINFORCE_FILL_OR_KILL = 'FOK'

    def __init__(self, api_key, api_secret, passphrase, sandbox=False):
        self.API_KEY = api_key
        self.API_SECRET = api_secret
        self.API_PASSPHRASE = passphrase
        if sandbox:
            self.API_URL = self.SANDBOX_API_URL
        else:
            self.API_URL = self.REST_API_URL

    async def async_request(self, client: httpx.AsyncClient, url: str, method: str, **kwargs):
        try:
            resp = await client.request(method, url, **kwargs)
        except Exception as e:
            raise Exception(type(e).__name__ + " " + str(e))
        return resp.text

    @staticmethod
    def _get_params_for_sig(data):
        try:
            return '&'.join(["{}={}".format(key, data[key]) for key in data])
        except Exception as e:
            raise KucoinGetParamsForSigFailed(type(e).__name__ + " " + str(e))

    def _generate_signature(self, nonce, method, path, data):
        try:
            data_json = ""
            endpoint = path
            if method == "get":
                if data:
                    query_string = self._get_params_for_sig(data)
                    endpoint = "{}?{}".format(path, query_string)
            elif data:
                data_json = compact_json_dict(data)
            sig_str = ("{}{}{}{}".format(nonce, method.upper(), endpoint, data_json)).encode('utf-8')
            m = hmac.new(self.API_SECRET.encode('utf-8'), sig_str, hashlib.sha256)
            return base64.b64encode(m.digest())
        except Exception as e:
            raise KucoinGenerateSignitureFailed(type(e).__name__ + " " + str(e))

    def _create_path(self, path, api_version=None):
        try:
            api_version = api_version or self.API_VERSION
            return '/api/{}/{}'.format(api_version, path)
        except Exception as e:
            raise KucoinCreatePathFailed(type(e).__name__ + " " + str(e))

    def _create_uri(self, path):
        try:
            return '{}{}'.format(self.API_URL, path)
        except Exception as e:
            raise KucoinCreateURIFailed(type(e).__name__ + " " + str(e))

    async def _request(self, client, method, path, signed, api_version=None, **kwargs):
        try:
            kwargs['timeout'] = 10
            kwargs['data'] = kwargs.get('data', {})
            kwargs['headers'] = kwargs.get('headers', {})

            full_path = self._create_path(path, api_version)
            uri = self._create_uri(full_path)

            if signed:
                # generate signature
                nonce = int(time.time() * 1000)
                kwargs['headers']['KC-API-TIMESTAMP'] = str(nonce)
                kwargs['headers']['KC-API-SIGN'] = self._generate_signature(nonce, method, full_path, kwargs['data'])

            if kwargs['data'] and method == 'get':
                kwargs['params'] = kwargs['data']
                del kwargs['data']

            if signed and method != 'get' and kwargs['data']:
                kwargs['data'] = compact_json_dict(kwargs['data'])
            kwargs['headers'].update({'Accept': 'application/json',
                                      'User-Agent': 'python_kucoin_local',
                                      'Content-Type': 'application/json',
                                      'KC-API-KEY': self.API_KEY,
                                      'KC-API-PASSPHRASE': self.API_PASSPHRASE})
            response = await self.async_request(client, uri, method, **kwargs)
            # print("response: ", response)
            return self._handle_response(response)
        except KucoinOrderNotExistOrNotAllowedToCancel as e:
            raise KucoinOrderNotExistOrNotAllowedToCancel()
        except Exception as e:
            raise KucoinRequestFailed(type(e).__name__ + " " + str(e))

    @staticmethod
    def _handle_response(response):
        try:
            res = json.loads(response)
        except Exception as e:
            raise KucoinApiResponseNotConvertibleToDict(type(e).__name__ + " " + str(e))
        if 'msg' in res and res['msg'] == 'order_not_exist_or_not_allow_to_cancel':
            raise KucoinOrderNotExistOrNotAllowedToCancel()
        if 'code' in res and res['code'] != "200000":
            raise KucoinApiResponseCodeNot200000("kucoin response code: " + res['code'] + (" message: " + res['msg']) if 'msg' in res else "")
        if 'success' in res and not res['success']:
            raise KucoinApiResponseSuccessFieldNotTrue(("kucoin response, code: " + res['code']) if 'code' in res else "" + (" ,message " + res['msg']) if 'msg' in res else "")
        if 'data' in res:
            res = res['data']
        return res

    async def _get(self, client, path, signed=False, api_version=None, **kwargs):
        return await self._request(client, 'get', path, signed, api_version, **kwargs)

    async def _post(self, client, path, signed=False, api_version=None, **kwargs):
        return await self._request(client, 'post', path, signed, api_version, **kwargs)

    async def _put(self, client, path, signed=False, api_version=None, **kwargs):
        return await self._request(client, 'put', path, signed, api_version, **kwargs)

    async def _delete(self, client, path, signed=False, api_version=None, **kwargs):
        return await self._request(client, 'delete', path, signed, api_version, **kwargs)

    # Server Endpoints
    async def get_timestamp(self, client):
        try:
            return await self._get(client, "timestamp")
        except Exception as e:
            raise KucoinGetTimeStampFailed(type(e).__name__ + " " + str(e))

    async def get_status(self, client):
        try:
            return await self._get(client, "status")
        except Exception as e:
            raise KucoinGetStatusFailed(type(e).__name__ + " " + str(e))

    # Currency Endpoints

    async def get_currencies(self, client):
        try:
            return await self._get(client, 'currencies')
        except Exception as e:
            raise KucoinGetCurrenciesFailed(type(e).__name__ + " " + str(e))

    async def get_currency(self, client, currency):
        try:
            return await self._get(client, 'currencies/{}'.format(currency))
        except Exception as e:
            raise KucoinGetCurrencyFailed(type(e).__name__ + " " + str(e))

    # User Account Endpoints

    async def get_non_zero_trade_accounts(self, client, kucoin_currency_name_to_coin_name):
        try:
            accounts = await self.get_accounts(client, account_type='trade')
            balances = {}
            for account in accounts:
                try:
                    currency_name = account['currency']
                    # balance = float(account['balance'])
                    available_balance = float(account['available'])
                    if available_balance > 0:
                        balances[kucoin_currency_name_to_coin_name[currency_name]] = available_balance
                except Exception as e:
                    pass
            return balances
        except Exception as e:
            raise KucoinGetNonZeroTradeAccountsFailed(type(e).__name__ + " " + str(e))

    async def get_accounts(self, client, currency=None, account_type=None):
        try:
            data = {}
            if currency:
                data['currency'] = currency
            if account_type:
                data['type'] = account_type

            return await self._get(client, 'accounts', True, data=data)
        except Exception as e:
            raise KucoinGetAccountsFailed(type(e).__name__ + " " + str(e))

    async def get_actual_fee(self, client, symbol):
        try:
            data = {
                'symbols': symbol
            }
            return await self._get(client, "trade-fees", True, data=data)
        except Exception as e:
            raise KucoinGetActualFeeFailed(type(e).__name__ + " " + str(e))

    async def get_account(self, client, account_id):
        try:
            return await self._get(client, 'accounts/{}'.format(account_id), True)
        except Exception as e:
            raise KucoinGetAccountFailed(type(e).__name__ + " " + str(e))

    async def create_account(self, client, account_type, currency):
        try:
            data = {
                'type': account_type,
                'currency': currency
            }
            return await self._post(client, 'accounts', True, data=data)
        except Exception as e:
            raise KucoinCreateAccountFailed(type(e).__name__ + " " + str(e))

    async def get_account_activity(self, client, currency=None, direction=None, biz_type=None, start=None, end=None,
                                   page=None, limit=None):
        try:
            data = {}
            if currency:
                data['currency'] = currency
            if direction:
                data['direction'] = direction
            if biz_type:
                data['bizType'] = biz_type
            if start:
                data['startAt'] = start
            if end:
                data['endAt'] = end
            if page:
                data['currentPage'] = page
            if limit:
                data['pageSize'] = limit

            return await self._get(client, 'accounts/ledgers', True, data=data)
        except Exception as e:
            raise KucoinGetAccountActivityFailed(type(e).__name__ + " " + str(e))

    async def create_inner_transfer(self, client, currency, from_type, to_type, amount, order_id=None):
        try:
            data = {
                'currency': currency,
                'from': from_type,
                'to': to_type,
                'amount': amount,
                'clientOid': order_id or flat_uuid(),
            }
            return await self._post(client, 'accounts/inner-transfer', True, api_version=self.API_VERSION2, data=data)
        except Exception as e:
            raise KucoinCreateInnerTransferFailed(type(e).__name__ + " " + str(e))

    # Deposit Endpoints

    async def create_deposit_address(self, client, currency, chain=None):
        try:
            data = {
                'currency': currency
            }

            if chain is not None:
                data['chain'] = chain

            return await self._post(client, 'deposit-addresses', True, data=data)
        except Exception as e:
            raise KucoinCreateDepositAddressFailed(type(e).__name__ + " " + str(e))

    async def get_deposit_address(self, client, currency):
        try:
            data = {
                'currency': currency
            }
            return await self._get(client, 'deposit-addresses', True, api_version=self.API_VERSION2, data=data)
        except Exception as e:
            raise KucoinGetDepositAddressFailed(type(e).__name__ + " " + str(e))

    async def get_deposits(self, client, currency=None, status=None, start=None, end=None, page=None, limit=None):
        try:
            data = {}
            if currency:
                data['currency'] = currency
            if status:
                data['status'] = status
            if start:
                data['startAt'] = start
            if end:
                data['endAt'] = end
            if limit:
                data['pageSize'] = limit
            if page:
                data['currentPage'] = page

            return await self._get(client, 'deposits', True, data=data)
        except Exception as e:
            raise KucoinGetDepositsFailed(type(e).__name__ + " " + str(e))

    # Withdraw Endpoints

    async def get_withdrawals(self, client, currency=None, status=None, start=None, end=None, page=None, limit=None):
        try:
            data = {}
            if currency:
                data['currency'] = currency
            if status:
                data['status'] = status
            if start:
                data['startAt'] = start
            if end:
                data['endAt'] = end
            if limit:
                data['pageSize'] = limit
            if page:
                data['currentPage'] = page
            return await self._get(client, 'withdrawals', True, data=data)
        except Exception as e:
            raise KucoinGetWithdrawalsFailed(type(e).__name__ + " " + str(e))

    async def get_withdrawal_quotas(self, client, currency):
        try:
            data = {
                'currency': currency
            }
            return await self._get(client, 'withdrawals/quotas', True, data=data)
        except Exception as e:
            raise KucoinGetWithdrawalQuotasFailed(type(e).__name__ + " " + str(e))

    async def create_withdrawal(self, client, currency, amount, address, memo=None, is_inner=False, remark=None, chain=None):
        try:
            data = {
                'currency': currency,
                'amount': amount,
                'address': address
            }

            if memo:
                data['memo'] = memo
            if is_inner:
                data['isInner'] = is_inner
            if remark:
                data['remark'] = remark
            if chain:
                data['chain'] = chain

            return await self._post(client, 'withdrawals', True, data=data)
        except Exception as e:
            raise KucoinCreateWithdrawFailed(type(e).__name__ + " " + str(e))

    async def cancel_withdrawal(self, client, withdrawal_id):
        try:
            return await self._delete(client, 'withdrawals/{}'.format(withdrawal_id), True)
        except Exception as e:
            raise KucoinCancelWithdrawalFailed(type(e).__name__ + " " + str(e))

    # Order Endpoints

    async def create_market_order(
            self, client, symbol, side, size=None, funds=None, client_oid=None, remark=None, stp=None, trade_type=None
    ):
        try:
            if not size and not funds:
                raise KucoinMarketOrderParamsException('Need size or fund parameter')

            if size and funds:
                raise KucoinMarketOrderParamsException('Need size or fund parameter not both')

            data = {
                'side': side,
                'symbol': symbol,
                'type': self.ORDER_MARKET,
                'clientOid': client_oid or flat_uuid()
            }

            if size:
                data['size'] = size
            if funds:
                data['funds'] = funds
            if remark:
                data['remark'] = remark
            if stp:
                data['stp'] = stp
            if trade_type:
                data['tradeType'] = trade_type

            return await self._post(client, 'orders', True, data=data)
        except Exception as e:
            raise KucoinCreateMarketOrderFailed("symbol: " + str(symbol) + " " + type(e).__name__ + " " + str(e))

    async def create_limit_order(self, client, symbol, side, price, size, client_oid=None, remark=None,
                                 time_in_force=None, stop=None, stop_price=None, stp=None, trade_type=None,
                                 cancel_after=None, post_only=None,
                                 hidden=None, iceberg=None, visible_size=None):
        try:
            if stop and not stop_price:
                raise KucoinLimitOrderParamsException('Stop order needs stop_price')

            if stop_price and not stop:
                raise KucoinLimitOrderParamsException('Stop order type required with stop_price')

            if cancel_after and time_in_force != self.TIMEINFORCE_GOOD_TILL_TIME:
                raise KucoinLimitOrderParamsException('Cancel after only works with time_in_force = "GTT"')

            if hidden and iceberg:
                raise KucoinLimitOrderParamsException('Order can be either "hidden" or "iceberg"')

            if iceberg and not visible_size:
                raise KucoinLimitOrderParamsException('Iceberg order requires visible_size')

            data = {'symbol': symbol, 'side': side, 'type': self.ORDER_LIMIT, 'price': price, 'size': size,
                    'clientOid': client_oid or flat_uuid()}

            if remark:
                data['remark'] = remark
            if stp:
                data['stp'] = stp
            if trade_type:
                data['tradeType'] = trade_type
            if time_in_force:
                data['timeInForce'] = time_in_force
            if cancel_after:
                data['cancelAfter'] = cancel_after
            if post_only:
                data['postOnly'] = post_only
            if stop:
                data['stop'] = stop
                data['stopPrice'] = stop_price
            if hidden:
                data['hidden'] = hidden
            if iceberg:
                data['iceberg'] = iceberg
                data['visible_size'] = visible_size

            return await self._post(client, 'orders', True, data=data)
        except Exception as e:
            raise KucoinCreateLimitOrderFailed("symbol: " + str(symbol) + " " + type(e).__name__ + " " + str(e))

    async def cancel_order(self, client, order_id):
        try:
            return await self._delete(client, 'orders/{}'.format(order_id), True)
        except KucoinOrderNotExistOrNotAllowedToCancel as e:
            raise KucoinOrderNotExistOrNotAllowedToCancel("order_id: " + str(order_id) + " " + type(e).__name__ + " " + str(e))
        except Exception as e:
            raise KucoinCancelOrderFailed("order_id: " + str(order_id) + " " + type(e).__name__ + " " + str(e))

    async def cancel_order_by_client_oid(self, client, client_oid):
        try:
            return await self._delete(client, 'order/client-order/{}'.format(client_oid), True)
        except KucoinOrderNotExistOrNotAllowedToCancel as e:
            raise KucoinOrderNotExistOrNotAllowedToCancel("client_oid: " + str(client_oid) + " " + type(e).__name__ + " " + str(e))
        except Exception as e:
            raise KucoinCancelOrderByClientOidFailed("client_oid: " + str(client_oid) + " " + type(e).__name__ + " " + str(e))

    async def cancel_all_orders(self, client, symbol=None):
        try:
            data = {}
            if symbol is not None:
                data['symbol'] = symbol
            return await self._delete(client, 'orders', True, data=data)
        except Exception as e:
            raise KucoinCancelAllOrdersFailed(type(e).__name__ + " " + str(e))

    async def get_orders(self, client, symbol=None, status=None, side=None, order_type=None,
                         start=None, end=None, page=None, limit=None, trade_type='TRADE'):
        try:
            data = {}
            if symbol:
                data['symbol'] = symbol
            if status:
                data['status'] = status
            if side:
                data['side'] = side
            if order_type:
                data['type'] = order_type
            if start:
                data['startAt'] = start
            if end:
                data['endAt'] = end
            if page:
                data['currentPage'] = page
            if limit:
                data['pageSize'] = limit
            if trade_type:
                data['tradeType'] = trade_type
            return await self._get(client, 'orders', True, data=data)
        except Exception as e:
            raise KucoinGetOrdersFailed(type(e).__name__ + " " + str(e))

    async def get_order(self, client, order_id):
        try:
            return await self._get(client, 'orders/{}'.format(order_id), True)
        except Exception as e:
            raise KucoinGetOrderFailed("order_id: " + str(order_id) + " " + type(e).__name__ + " " + str(e))

    async def get_order_by_client_oid(self, client, client_oid):
        try:
            return await self._get(client, 'order/client-order/{}'.format(client_oid), True)
        except Exception as e:
            raise KucoinGetOrderByClientOid("client_oid: " + client_oid + " " + type(e).__name__ + " " + str(e))

    async def get_fills(self, client, order_id=None, symbol=None, side=None, order_type=None,
                        start=None, end=None, page=None, limit=None, trade_type=None):
        try:
            data = {}
            if order_id:
                data['orderId'] = order_id
            if symbol:
                data['symbol'] = symbol
            if side:
                data['side'] = side
            if order_type:
                data['type'] = order_type
            if start:
                data['startAt'] = start
            if end:
                data['endAt'] = end
            if page:
                data['currentPage'] = page
            if limit:
                data['pageSize'] = limit
            if trade_type:
                data['tradeType'] = trade_type

            return await self._get(client, 'fills', True, data=data)
        except Exception as e:
            raise KucoinGetFillsFailed(type(e).__name__ + " " + str(e))

    # Market Endpoints

    async def get_symbols(self, client):
        try:
            return await self._get(client, 'symbols', False)
        except Exception as e:
            raise KucoinGetSymbolsFailed(type(e).__name__ + " " + str(e))

    async def get_ticker(self, client, symbol=None):
        try:
            data = {}
            tick_path = 'market/allTickers'
            if symbol is not None:
                tick_path = 'market/orderbook/level1'
                data = {
                    'symbol': symbol
                }
            return await self._get(client, tick_path, False, data=data)
        except Exception as e:
            raise KucoinGetTickerFailed(type(e).__name__ + " " + str(e))

    async def get_fiat_prices(self, client, base=None, symbol=None):
        try:
            data = {}
            if base is not None:
                data['base'] = base
            if symbol is not None:
                data['currencies'] = symbol

            return await self._get(client, 'prices', False, data=data)
        except Exception as e:
            raise KucoinGetFiatPricesFailed(type(e).__name__ + " " + str(e))

    async def get_24hr_stats(self, client, symbol):
        # returned volume is in base currency units
        try:
            data = {
                'symbol': symbol
            }
            return await self._get(client, 'market/stats', False, data=data)
        except Exception as e:
            raise KucoinGet24HourStatsFailed(type(e).__name__ + " " + str(e))

    async def get_markets(self, client):
        try:
            return await self._get(client, 'markets', False)
        except Exception as e:
            raise KucoinGetMarketsFailed(type(e).__name__ + " " + str(e))

    async def get_order_book(self, client, symbol, depth_20=False):
        # returns orderbook in depth 20 or 100
        try:
            data = {
                'symbol': symbol
            }
            path = 'market/orderbook/level2_'
            if depth_20:
                path += '20'
            else:
                path += '100'
            response = await self._get(client, path, False, data=data)
            if 'asks' not in response or 'bids' not in response:
                raise KucoinOrderBookFormatError("ask or bid not in order_book")

            if (not isinstance(response['asks'], list)) or (not isinstance(response['bids'], list)):
                raise KucoinOrderBookFormatError("kucoin order_book asks or bids is not a list")
            res = {}
            res['asks'] = [(lambda x: list(map(float, x[0:2])))(ask) for ask in response['asks']]
            res['bids'] = [(lambda x: list(map(float, x[0:2])))(bid) for bid in response['bids']]
            res['asks'].sort(key=lambda x: x[0])
            res['bids'].sort(key=lambda x: -x[0])
            return res
        except Exception as e:
            raise KucoinGetOrderBookFailed("symbol:" + symbol + " " + type(e).__name__ + " " + str(e))

    async def get_full_order_book(self, client, symbol):
        # returns orderbook in max depth
        try:
            data = {
                'symbol': symbol
            }
            return await self._get(client, 'market/orderbook/level2', True, api_version=self.API_VERSION3, data=data)
        except Exception as e:
            raise KucoinGetFullOrderBookFailed(type(e).__name__ + " " + str(e))

    async def get_trade_histories(self, client, symbol):
        try:
            data = {
                'symbol': symbol
            }

            return await self._get(client, 'market/histories', False, data=data)
        except Exception as e:
            raise KucoinGetTradeHistoriesFailed(type(e).__name__ + " " + str(e))

    async def get_kline_data(self, client, symbol, kline_type=None, start=None, end=None):
        """Get kline data

        For each query, the system would return at most 1500 pieces of data.
        To obtain more data, please page the data by time.

        :param symbol: Name of symbol e.g. KCS-BTC
        :type symbol: string
        :param kline_type: type of symbol, type of candlestick patterns: 1min, 3min, 5min, 15min, 30min, 1hour, 2hour,
                           4hour, 6hour, 8hour, 12hour, 1day, 1week
        :type kline_type: string
        :param start: Start time as unix timestamp (optional) default start of day in UTC
        :type start: int
        :param end: End time as unix timestamp (optional) default now in UTC
        :type end: int

        https://docs.kucoin.com/#get-historic-rates

        .. code:: python

            klines = client.get_kline_data('KCS-BTC', '5min', 1507479171, 1510278278)

        :returns: ApiResponse

        .. code:: python

            [
                [
                    "1545904980",             //Start time of the candle cycle
                    "0.058",                  //opening price
                    "0.049",                  //closing price
                    "0.058",                  //highest price
                    "0.049",                  //lowest price
                    "0.018",                  //Transaction amount
                    "0.000945"                //Transaction volume
                ],
                [
                    "1545904920",
                    "0.058",
                    "0.072",
                    "0.072",
                    "0.058",
                    "0.103",
                    "0.006986"
                ]
            ]

        :raises: KucoinResponseException, KucoinAPIException

        """
        try:
            data = {
                'symbol': symbol
            }

            if kline_type is not None:
                data['type'] = kline_type
            else:
                data['type'] = '5min'

            if start is not None:
                data['startAt'] = start
            else:
                data['startAt'] = calendar.timegm(datetime.utcnow().date().timetuple())

            if end is not None:
                data['endAt'] = end
            else:
                data['endAt'] = int(time.time())

            return await self._get(client, 'market/candles', False, data=data)
        except Exception as e:
            raise KucoinGetKlineFailed(type(e).__name__ + " " + str(e))

    # Websocket Endpoints

    async def get_ws_endpoint(self, client, private=False):
        try:
            path = 'bullet-public'
            signed = private
            if private:
                path = 'bullet-private'

            return await self._post(client, path, signed)
        except Exception as e:
            raise KucoinGetWsEndpointFailed(type(e).__name__ + " " + str(e))
